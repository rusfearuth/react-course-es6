// @flow
import axios from 'axios';
import Promises from 'bluebird';

type Loader = (username: string) => Promises
type PlayersLoader = (players: Array<string>) => Promises
type Battle = (players: Array<Object>) => Promises

const repos: Loader = (username) => axios
  .get(`https://api.github.com/users/${username}/repos`, {
    params: {
      per_page: 100
    }
  });

const totalStars: (repos: Array<Object>) => number = (repos) => {
  if (!repos.data)
  {
    return 0;
  }

  return repos.data.reduce((prev: number, current: Object) => {
    return prev + current.stargazers_count
  }, 0);
}

const playerInfo: (player: { login: string }) => Promises = (player) => repos(player.login)
  .then(totalStars)
  .then(totalStars => {
    return {
      followers: player.followers,
      totalStars: totalStars
    }
  })

const calculateScores: (players: Array<Object>) => Array<Object> = (players) => [
  players[0].followers * 3 + players[0].totalStars,
  players[1].followers * 3 + players[1].totalStars
]

export const getUserInfo: Loader = (username) => axios.get(`https://api.github.com/users/${username}`)

export const getPlayersInfo: PlayersLoader = (players) => {
  return axios.all(
    players.map(
      username => {
        return getUserInfo(username);
      }
    )
  ).then(info => {
    return info.map(user => {
      return user.data
    });
  });
};

export const battle: Battle = (players) => {
  let first = playerInfo(players[0])
  let second = playerInfo(players[1])

  return Promises.all([ first, second ])
    .then(calculateScores)
    .catch(err => {
      console.warn(`playerInfo function error: ${err}`);
    })
}
