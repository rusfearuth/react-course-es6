/* @flow */
import { Main, Home, Error } from '../components';
import { PromptContainer, ConfirmBattleContainer, ResultsContainer } from '../containers';

export const router = {
  path: '/',
  component: Main,
  indexRoute: { component: Home },
  childRoutes: [
    { path: 'home', component: Home },
    { path: 'playerOne', header: 'Player One', component: PromptContainer },
    { path: 'playerTwo/:playerOne', header: 'Player Two', component: PromptContainer },
    { path: 'battle', component: ConfirmBattleContainer },
    { path: 'results', component: ResultsContainer },
    { path: '*', component: Error }
  ]
}
