// @flow
import React from 'react';
import { Link } from 'react-router';
import { space } from '../styles';
import { Loading } from './loading';
import { UserDetails } from './user-details';
import { UserDetailsWrapper } from './user-details-wrapper';
import { MainContainer } from './main-container';
import type { UIComponent, UIComponentWithoutProps } from './common';

type ResultsProps = {
  isLoading: boolean,
  playersInfo: Array<Object>,
  scores: Array<number>
}

const StartOver: UIComponentWithoutProps = () => {
  return (
    <div className="col-sm-12" style={ space }>
      <Link to="playerOne">
        <button type="button" className="btn btn-lg btn-danger">
          Start Over
        </button>
      </Link>
    </div>
  );
};

export const Results: UIComponent<ResultsProps> = (props) => {
  if (!props.isLoading && props.scores.length < 2)
  {
      return (
        <MainContainer header="It's a tie!">
          <StartOver />
        </MainContainer>
      );
  }

  console.log(props.isLoading, props);
  let winnerIndex = props.scores[0] > props.scores[1] ? 0: 1;
  let loserIndex = winnerIndex === 0 ? 1: 0;
  console.log(winnerIndex, loserIndex);

  return props.isLoading
  ? (<Loading speed={ 100 } text='One Moment' />)
  : (
    <MainContainer header="Results">
      <div className="col-sm-8 col-sm-offset-2">
        <UserDetailsWrapper header="Winner">
          <UserDetails score={ props.scores[winnerIndex] } info={ props.playersInfo[winnerIndex] } />
        </UserDetailsWrapper>
        <UserDetailsWrapper header="Loser">
          <UserDetails score={ props.scores[loserIndex] } info={ props.playersInfo[loserIndex] } />
        </UserDetailsWrapper>
      </div>
      <StartOver />
    </MainContainer>
  );
}
