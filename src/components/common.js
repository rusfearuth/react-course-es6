// @flow

export type UIComponent <T> = (props: T) => any;
export type UIComponentWithoutProps = () => any;
