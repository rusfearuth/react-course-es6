// @flow
import React from 'react';
import { Link } from 'react-router';
import { MainContainer } from './main-container';
import type { UIComponent } from './common';

type Props = {}

export const Home: UIComponent<Props> = (props) => {
  return (
      <MainContainer header="Github Battle">
        <p className="lead">Some fancy motto</p>
        <Link to='/playerOne'>
          <button type="button" className="btn btn-lg btn-success">
            Get started
          </button>
        </Link>
      </MainContainer>
    );
}
