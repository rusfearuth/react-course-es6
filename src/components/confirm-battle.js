// @flow
import React from 'react';
import { Link } from 'react-router';
import type { UIComponent } from './common';
import { MainContainer } from './main-container';
import { Loading } from './loading';
import { space } from '../styles';
import { UserDetails } from './user-details';
import { UserDetailsWrapper } from './user-details-wrapper';

type Props = {
  isLoading: boolean,
  onInitBattle: Function,
  playerInfo: Array<any>
}

export const ConfirmBattle: UIComponent<Props> = (props) => {
  return props.isLoading === true
  ? (
    <Loading speed={ 800 } text='Waiting' />
  )
  : (
    <MainContainer header="Confirm Players">
      <div className="col-sm-8 col-sm-offset-2">
        <UserDetailsWrapper header="Player One">
          <UserDetails info={props.playerInfo[0]} />
        </UserDetailsWrapper>
        <UserDetailsWrapper header="Player Two">
          <UserDetails info={props.playerInfo[1]} />
        </UserDetailsWrapper>
      </div>
      <div className="col-sm-8 col-sm-offset-2">
        <div className="col-sm-12" style={ space }>
          <button type="button" className="btn btn-lg btn-success" onClick={props.onInitBattle}>
            Initiate Battle!
          </button>
        </div>
        <div className="col-sm-12" style={ space }>
          <Link to="/playerOne">
            <button type="button" className="btn btn-lg btn-danger">
              Reselect Players
            </button>
          </Link>
        </div>
      </div>
    </MainContainer>
  )
};
