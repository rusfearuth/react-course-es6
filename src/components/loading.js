// @flow
import React, { Component } from 'react';

type LoadingState = {
  originalText: string,
  text: string
}

type LoadingProps = {
    text: string,
    speed: number
}

const defaultStyles = {
  container: {
    position: 'fixed',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    fontSize: '55px'
  },
  content: {
    textAlign: 'center',
    position: 'absolute',
    width: '100%',
    marginTop: '30px'
  }
};

export class Loading extends Component
{
  props: LoadingProps
  state: LoadingState
  interval: number

  static defaultProps = {
    text: 'Loading',
    speed: 300
  }

  constructor(props: Object)
  {
    super(props);
    this.state = {
      originalText: this.props.text,
      text: this.props.text
    }
  }

  componentDidMount()
  {
    const stopOn = `${this.state.originalText}...`;
    this.interval = setInterval(() => {
      if (this.state.text === stopOn)
      {
        this.setState({
          text: this.state.originalText
        })
      }
      else
      {
        this.setState({
          text: `${this.state.text}.`
        });
      }
    }, this.props.speed);
  }

  componentWillUnmount()
  {
    clearInterval(this.interval);
  }

  render() {
      return (
        <div style={ defaultStyles.container }>
          <p style={ defaultStyles.content }>{ this.state.text }</p>
        </div>
      );
  }
}
