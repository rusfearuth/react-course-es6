// @flow
import React from 'react';
import type { UIComponent } from './common';

type UserProps = {
  score?: number,
  info: {
    avatar_url: string,
    name: string,
    login: string,
    followers: number,
    following: number,
    public_repos: number,
    location?: string,
    company?: string,
    blog?: string
  }
}

export const UserDetails: UIComponent<UserProps> = (props) => {
  return (
    <div>
      { props.score && <li className="list-group-item"><h3>Score: { props.score }</h3></li>}
      <li className="list-group-item"><img src={ props.info.avatar_url } className="img-rounded img-responsive" role="presentation"/> </li>
      { props.info && <li className="list-group-item">Name: { props.info.name }</li>}
      <li className="list-group-item">Username: { props.info.login }</li>
      { props.info.location && <li className="list-group-item">Location: { props.info.location }</li> }
      { props.info.company && <li className="list-group-item">Company: { props.info.company }</li> }
      <li className="list-group-item">Followers: { props.info.followers }</li>
      <li className="list-group-item">Following: { props.info.following }</li>
      <li className="list-group-item">Public Repos: { props.info.public_repos }</li>
      { props.info.blog && <li className="list-group-item">Blog: { props.info.blog }</li>}
    </div>
  );
}
