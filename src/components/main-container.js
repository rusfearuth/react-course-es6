// @flow
import React from 'react';
import { transparentBg } from '../styles';
import type { UIComponent } from './common';

type MainContainerProps = {
  header: string
};

export const MainContainer: UIComponent<MainContainerProps> = (props) => {
  return (
      <div className="jumbotron col-sm-12 text-center" style={ transparentBg }>
        <h1>{ props.header }</h1>
        { props.children }
      </div>
  );
};
