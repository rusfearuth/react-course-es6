// @flow
import React from 'react';
import type { UIComponent } from './common';

type UserDetailsWrapperProps = {
  header: string,
  children?: any
}

export const UserDetailsWrapper: UIComponent<UserDetailsWrapperProps> = (props) => {
  return (
    <div className="col-sm-6">
      <p className="lead">{ props.header }</p>
      { props.children }
    </div>
  );
}
