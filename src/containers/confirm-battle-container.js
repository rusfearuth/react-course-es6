// @flow
import React, { Component } from 'react';
import { ConfirmBattle } from '../components';
import { getPlayersInfo } from '../utils';

type CBState = {
  isLoading: boolean,
  playerInfo: Array<any>
}

export class ConfirmBattleContainer extends Component
{
  state: CBState;

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props: Object) {
    super(props);

    this.state = {
      isLoading: true,
      playerInfo: []
    };

    this.handleInitBattle = this.handleInitBattle.bind(this)
  }

  componentDidMount() {
    let { playerOne, playerTwo } = this.props.location.query;
    getPlayersInfo([playerOne, playerTwo]).then(players => {
      this.setState({
        isLoading: false,
        playerInfo: [ ...players ]
      });
    });
  }

  handleInitBattle() {

    this.context.router.push({
      pathname: '/results',
      state: {
        playerInfo: [ ...this.state.playerInfo ]
      }
    })
  }

  render() {
    return (
      <ConfirmBattle
        isLoading={this.state.isLoading}
        onInitBattle={this.handleInitBattle}
        playerInfo={this.state.playerInfo}
        />
    );
  }
}
