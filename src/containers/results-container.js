// @flow
import React, { Component } from 'react';
import { Results } from '../components';
import { battle } from '../utils';

type State = {
  isLoading: boolean,
  playerInfo: Array<Object>,
  scores: Array<number>
}

export class ResultsContainer extends Component
{
  state: State

  // eslint-disable-next-line
  constructor(props: Object)
  {
    super(props);

    this.state = {
      isLoading: true,
      playerInfo: [],
      scores: []
    };
  }

  componentDidMount()
  {
    battle(this.props.location.state.playerInfo).then(scores => {
      this.setState({
        isLoading: false,
        playerInfo: [ ...this.props.location.state.playerInfo],
        scores: [ ...scores ]
      });
    });
  }

  render()
  {
    return (
      <Results
          isLoading={ this.state.isLoading }
          playersInfo={ this.props.location.state.playerInfo }
          scores={ this.state.scores }
          />
    );
  }
}
