import React, { Component } from 'react';
import { Prompt } from '../components';

export class PromptContainer extends Component
{
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props)
  {
    super(props);

    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleSubmitUser = this.handleSubmitUser.bind(this);

    this.state = {
      username: ''
    };
  }

  handleUpdateUser(event)
  {
    console.log(event.target.value);
    this.setState({
      username: event.target.value
    });
  }

  handleSubmitUser(e)
  {
    e.preventDefault();
    const username = this.state.username;

    this.setState({
      username: ''
    });

    console.log(username)

    if (this.props.routeParams.playerOne)
    {
      this.context.router.push({
        pathname: '/battle',
        query: {
          playerOne: this.props.routeParams.playerOne,
          playerTwo: username
        }
      });
    }
    else
    {
      this.context.router.push(`/playerTwo/${username}`);
    }
  }

  render() {
    return (
      <Prompt
        header={ this.props.route.header }
        onSubmitUser={ this.handleSubmitUser }
        onUpdateUser={ this.handleUpdateUser }
        username={ this.state.username }
        />
    );
  }
}
