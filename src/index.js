// @flow
import React from 'react';
import { Router, hashHistory } from 'react-router';
import { render } from 'react-dom';
import { router } from './config';

render(
  <Router history={ hashHistory } routes={ router } />,
  document.getElementById('root')
);
